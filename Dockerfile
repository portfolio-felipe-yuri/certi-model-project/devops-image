FROM debian

# Uptade repositories
RUN apt update -y

# Download and install utils
RUN apt install curl -y

# Download and install jdk 17
RUN curl https://download.oracle.com/java/17/archive/jdk-17.0.6_linux-x64_bin.tar.gz --output ./tmp/jdk-17.0.6_linux-x64_bin.tar.gz
RUN tar -xzvf ./tmp/jdk-17.0.6_linux-x64_bin.tar.gz -C ./opt/

# Download and install maven
RUN curl https://dlcdn.apache.org/maven/maven-3/3.9.0/binaries/apache-maven-3.9.0-bin.tar.gz --output ./tmp/apache-maven-3.9.0-bin.zip
RUN tar -xvzf ./tmp/apache-maven-3.9.0-bin.zip -C ./opt/

# Export environment variables
ENV JAVA_HOME=/opt/jdk-17.0.6
ENV PATH=$PATH:$JAVA_HOME/bin

ENV MAVEN_HOME=/opt/apache-maven-3.9.0
ENV PATH=$PATH:$MAVEN_HOME/bin

# Reload variables
RUN bash /etc/profile

# Install Docker
RUN apt-get update
RUN apt-get install ca-certificates gnupg lsb-release -y
RUN curl -fsSL https://get.docker.com -o get-docker.sh
RUN sh get-docker.sh
